import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from abc import abstractmethod
from functools import partial
from matplotlib.mlab import griddata


class BasePlotter:
    def __init__(self):
        self.active = True
        self.projection = None
        self.figure = None
        self.axis = None
        self.data = None

    @staticmethod
    def initialize(figure, layout, plotter_instance, plotter_index):
        plotter_instance.figure = figure
        plotter_instance.axis = figure.add_subplot(
            *layout, 
            plotter_index + 1,
            projection=plotter_instance.projection
        )
        return plotter_instance

    def stop_plotter(self):
        self.active = False

    def restart_plotter(self):
        self.active = True

    @abstractmethod
    def update_plot(self, *data):
        pass
    
    @abstractmethod
    def append_data(self, *data):
        pass
    
    @abstractmethod
    def plot_data(self):
        pass  
    
    @abstractmethod
    def clear_data(self, *clear_params):
        pass      


class BiVarPlotter(BasePlotter):
    def __init__(self, xlim=None, ylim=None, zlim=None):
        super().__init__()
        self.projection = '3d'
        self.first_run = True
        self.xlim = xlim
        self.ylim = ylim
        self.zlim = zlim
        self.clear_data()

    def clear_data(self):
        self.data = np.empty(
            (0, 3),
            dtype=np.float32
        )
        if self.axis is not None:
            self.axis.clear()

    def append_data(self, *data):
        if self.first_run is True:
            self.axis.set_xlim(self.xlim)
            self.axis.set_ylim(self.ylim)
            self.axis.set_zlim(self.zlim)
        self.data = np.append(
            self.data,
            np.array(*data).reshape(-1, 3),
            axis=0
        )

    def plot_data(self):
        if self.data.shape[0] < 10:
            return None

        return self.axis.plot_trisurf(
            self.data[:, 0].flatten(),
            self.data[:, 1].flatten(),
            self.data[:, 2].flatten()
        )

    def update_plot(self, *data, append=True):
        if append is False:
            self.clear_data()
        self.append_data(data)
        line_iter = self.plot_data()
        if line_iter is None:
            return

        self.figure.canvas.draw()
        self.figure.canvas.flush_events()


class ContourPlotter(BasePlotter):
    def __init__(self, xlim=None, ylim=None):
        super().__init__()
        self.first_run = True
        self.xlim = xlim
        self.ylim = ylim
        self.clear_data()

    def clear_data(self):
        self.data = np.empty(
            (0, 3),
            dtype=np.float32
        )
        if self.axis is not None:
            self.axis.clear()
    
    def append_data(self, *data):
        if self.first_run is True:
            self.axis.set_xlim(self.xlim)
            self.axis.set_ylim(self.ylim)
        self.data = np.append(
            self.data,
            np.array(*data).reshape(-1, 3),
            axis=0
        )

    def plot_data(self):
        xi = np.linspace(*self.xlim, 500)
        yi = np.linspace(*self.ylim, 500)
        zi = griddata(self.data[:, 0], self.data[:, 1], self.data[:, 2], xi, yi, interp='linear')
        return plt.contour(xi, yi, zi) 
        
    def update_plot(self, *data, append=True):
        if append is False:
            self.clear_data()
        self.append_data(data)
        self.plot_data()
        self.figure.canvas.draw()
        self.figure.canvas.flush_events()


class UniVarPlotter(BasePlotter):
    def __init__(self, xlim=None, ylim=None):
        super().__init__()
        self.first_run = True
        self.xlim = xlim
        self.ylim = ylim
        self.clear_data()

    def clear_data(self):
        self.data = np.empty(
            (0, 2),
            dtype=np.float32
        )
        if self.axis is not None:
            self.axis.clear()

    def append_data(self, *data):
        if self.first_run is True:
            self.axis.set_xlim(self.xlim)
            self.axis.set_ylim(self.ylim)
        self.data = np.append(
            self.data,
            np.array(*data).reshape(-1, 2),
            axis=0
        )

    def plot_data(self):
        return self.axis.plot(
            self.data[:, 0],
            self.data[:, 1]
        )

    def update_plot(self, *data, append=True):
        if append is False:
            self.clear_data()
        self.append_data(data)
        line, = self.plot_data()
        self.figure.canvas.draw()
        self.figure.canvas.flush_events()


class PlotterOrchestrator:
    def initialize(self, plotter):
        return self.plotter_initializer(
            self.figure, 
            self.layout,
            plotter[0],
            plotter[1]
        )

    def __init__(self, layout, *plotters, interactive=False):
        if interactive is True:
            plt.ion()
        self.layout = layout
        self.figure = plt.figure()
        self.plotter_initializer = type(plotters[0]).initialize
        self.plotters = list(map(
            self.initialize,
            list(zip(plotters, range(len(plotters))))
        ))
        
    def plot(self):
        for plotter in self.plotters:
            if plotter.active is False:
                continue
            plotter.plot_data()

    def dispatch_data(self, *data):
        for data_idx, data_elem in enumerate(data):
            if data_elem is None:
                continue

            self.plotters[data_idx].append_data(data_elem)
    
    def update_plot(self, *data):
        for data_idx, data_elem in enumerate(data):
            if data_elem is None or self.plotters[data_idx].active is False:
                continue

            self.plotters[data_idx].update_plot(data_elem)

    def clear_plot(self, *mask):
        for idx, plotter in enumerate(self.plotters):
            if plotter.active is False and mask[idx] is False:
                continue
            plotter.clear_data()


if __name__ == "__main__":
    pla = UniVarPlotter(xlim=(0, 100), ylim=(0, 5000))
    plb = UniVarPlotter(xlim=(0, 100), ylim=(0, 5000))
    plc = BiVarPlotter(xlim=(-1, 1), ylim=(-1, 1), zlim=(0, 1))
    layout = (2,2)
    orchestrator = PlotterOrchestrator(layout, pla, plb, plc, interactive=True)

    data_a = np.vstack(
        (
            np.arange(100),
            0.1 * np.power(np.arange(100), 2)
        )
    ).T
    data_b = np.vstack(
        (
            np.arange(100),
            0.01 * np.power(np.arange(100), 2)
        )
    ).T
    
    n_radii = 8
    n_angles = 36

    # Make radii and angles spaces (radius r=0 omitted to eliminate duplication).
    radii = np.linspace(0.125, 1.0, n_radii)
    angles = np.linspace(0, 2*np.pi, n_angles, endpoint=False)

    # Repeat all angles for each radius.
    angles = np.repeat(angles[..., np.newaxis], n_radii, axis=1)

    x = np.append(0, (radii*np.cos(angles)).flatten())
    y = np.append(0, (radii*np.sin(angles)).flatten())

    # Compute z to make the pringle surface.
    z = np.sin(-x*y)
    data_c = np.vstack(
        (
            x,
            y,
            z
        )
    ).T

    print(data_c.shape)

    # orchestrator.dispatch_data(data_a, data_b, data_c)
    for i in range(data_a.shape[0]):
        if i == 30:
            orchestrator.clear_plot(*[True, False])
        orchestrator.update_plot(data_a[i, :], data_b[i, :], None)
    # orchestrator.plot() 
    # plt.show()
