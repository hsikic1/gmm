import numpy as np
from animator import UniVarPlotter, BiVarPlotter, PlotterOrchestrator, ContourPlotter
from matplotlib.mlab import griddata
import matplotlib.pyplot as plt


class GmmClusterer:
    def __init__(self, **kwargs):
        self.__dict__.update(**kwargs)
        self.total_responsibilities = None
        self.last_likelihood = None
        self.responsibilities = np.empty(
            (self.components, self.samples.shape[0]),
            dtype=np.float32
        )

    def gaussian(self, x, mean, sigma):
        diff = (x - mean).reshape(
            -1,
            x.shape[1],
            1
        )   
        inner_product = diff.transpose(0, 2, 1) @ np.linalg.inv(sigma) @ diff
        normalizer = np.power(
            2 * np.pi, 
            mean.shape[0] / 2
        )
        det = np.sqrt(np.linalg.det(sigma))
        exponential = np.exp(-0.5 * inner_product)
        return (exponential / (normalizer * det)).reshape(-1)

    def ex_step(self):
        for comp in range(self.components):
            self.responsibilities[comp, :] = self.gaussian(
                self.samples,
                self.means[comp, :],
                self.covs[comp, :, :],
            )

        self.responsibilities *= self.mix_factors.reshape(-1, 1)
        self.responsibilities /= np.sum(
            self.responsibilities,
            axis=0
        )
        self.total_responsibilities = np.sum(
            self.responsibilities,
            axis=1
        )

    def max_step(self):
        self.mix_factors = self.total_responsibilities / self.samples.shape[0]
        for comp in range(self.components):
            diff = (self.samples - self.means[comp, :]).reshape(
                -1,
                self.samples.shape[1],
                1
            )   
            outer_prods = diff @ diff.transpose(0, 2, 1)
            self.covs[comp, :] = np.sum(
                self.responsibilities[comp, :].reshape(-1, 1, 1) * outer_prods,
                axis=0
            )
            self.means[comp, :] = self.responsibilities[comp, :] @ self.samples
            self.covs[comp, :, :] /= self.total_responsibilities[comp]
            self.means[comp, :] /= self.total_responsibilities[comp]

    def evaluate_convergence(self):
        likelihood_comps = np.empty(
            (
                self.components,
                self.samples.shape[0]
            )
        )
        for comp in range(self.components):
            likelihood_comps[comp, :] = self.gaussian(
                self.samples,
                self.means[comp, :],
                self.covs[comp, :, :],
            )
            likelihood_comps[comp, :] * self.mix_factors[comp]

        likelihood_comps = np.sum(
            likelihood_comps,
            axis=0
        )
        likelihood = np.sum(
            np.log(likelihood_comps),
            axis=0
        ) 

        if self.last_likelihood is None:
            self.last_likelihood = likelihood
            return False

        criteria = np.abs(likelihood - self.last_likelihood) < self.eps
        self.last_likelihood = likelihood
        return criteria

    def render_mixture(self):
        points = np.random.uniform(low=0,high=100,size=(1000,2))
        gaussians = clust.gaussian(points, self.means[0, :], self.covs[0, :, :]) * self.mix_factors[0]
        for i in range(1, self.components):
            gaussians += clust.gaussian(
                points, self.means[i, :], 
                self.covs[i, :, :]) * self.mix_factors[i] 

        data = np.concatenate(
            (points, np.log(gaussians).reshape(-1, 1)),
            axis=1
        )
        self.orchestrator.clear_plot(*[True])
        self.orchestrator.update_plot(data)

    def optimize(self):
        self.orchestrator = PlotterOrchestrator(
            (1, 1), 
            ContourPlotter(xlim=(0, 100), ylim=(0, 100)), 
            interactive=True
        )
        while True:
            self.ex_step()
            self.max_step()
            self.render_mixture()
            if self.evaluate_convergence() == True:
                break
        
        return self.means, self.covs, self.mix_factors


if __name__ == "__main__":
    samples = np.random.multivariate_normal([40,40], [[0.2, 0.1],[0.1, 0.2]], 600)
    samples = np.append(
        samples,
        np.random.multivariate_normal([10,10], [[0.3, 0.1],[0.1, 0.3]], 500),
        axis=0
    )
    samples = np.append(
        samples,
        np.random.multivariate_normal([80,80], [[0.7, -0.3],[-0.3, 0.7]], 600),
        axis=0
    )
    components = 3
    means = np.array(
        ((25.0, 25.0), 
        (-33.0, -33.0), 
        (50.0, 50.0)
    ))
    covs =  np.array([
        [
            [30.45, 0],
            [0, 30.45]
        ],
        [
            [50.5, 0],
            [0, 50.5]
        ],
        [
            [30.5, 0],
            [0, 30.5]
        ]
    ])
    mix_factors = np.array((0.9, 0.05, 0.05))
    clust = GmmClusterer(
        samples=samples,
        components=components,
        mix_factors=mix_factors,
        means=means,
        covs=covs,
        eps=1e-8
    )
    means, covs, mix = clust.optimize()
    plt.show()